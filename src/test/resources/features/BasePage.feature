Feature: Base Page

  Background:
    Given I open Fitekin base page

  @TEST
  Scenario Outline: Fitekin base page language and <countryName> selection
    And I click Country menu
    And I select <countryName> as Country in the list
    And I click Continue button
    And I check that Log in page is opened

    Examples:
    |   countryName     |
    |   Estonia         |
    |   Czech Republic  |