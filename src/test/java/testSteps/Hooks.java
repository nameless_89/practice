package testSteps;

import com.codeborne.selenide.WebDriverRunner;
import environment.EnvironmentSetup;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class Hooks {
    private final EnvironmentSetup environment;


    public Hooks (EnvironmentSetup environment) {
        this.environment = environment;
    }

    @Before
    @Given("I open Fitekin base page")
    public void openFitekInBasePage() {
        environment.setUp();
    }

    @After
    public void closeBrowser(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", "\\target\\cucumber-report.html");
        }
        environment.tearDown();
    }
}
