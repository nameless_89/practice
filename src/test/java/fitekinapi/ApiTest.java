package fitekinapi;

import com.browserup.harreader.model.HarEntry;
import environment.EnvironmentSetup;

import java.util.List;

public class ApiTest {

    private EnvironmentSetup environmentSetup;

    public ApiTest(EnvironmentSetup environmentSetup) {
        this.environmentSetup = environmentSetup;
    }

    public String authorizationToken;

    public void getCurrency() {

        String acceptHeader = "application/json";
        String requestUrl = "https://test.fitekin.com";
        List<HarEntry> requests = environmentSetup.bmp.getHar().getLog().getEntries();
        int requestSize;
        String headerContent = requests.get(10).getResponse().getContent().getText();

        /*if (headerContent.contains("IsBoAdmin")) {
            authorizationToken = headerContent.substring(10, 669);
        } else {
             while (!headerContent.contains("IsBoAdmin")) {
                 for(requestSize = requests.size(); requestSize == requests.size(); requestSize--) {
                     headerContent = requests.get(requestSize--).getResponse().getContent().getText();
                 }
             }
        }*/

        System.out.println("Token: " + authorizationToken);

        //String authorizationToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJCYWNrT2ZmaWNlVXNlckd1aWQiOiJiNTE0N2E0Mi1kMGFhLTQyZWMtOGY5YS0zNWY0YmRhNzAyNzIiLCJCYWNrT2ZmaWNlQ29tcGFueUd1aWQiOiIzMDlmYTdjOC1lYzQxLTRmNWMtOGRmMC02YTdlYzhhYTBhNjIiLCJMYW5ndWFnZSI6bnVsbCwiRXhwaXJhdGlvbkRhdGUiOiIyMDIxLTA4LTI3VDE3OjMzOjE5LjMwOTU5NzUrMDM6MDAiLCJEYlNlcnZlcklwIjoiMTAuMjI0LjAuMTE1IiwiRGJUeXBlIjoxLCJEYk5hbWUiOiJFbXIiLCJEYlNjaGVtYSI6IkVtcl8zMDlmYTdjOF9lYzQxXzRmNWNfOGRmMF82YTdlYzhhYTBhNjIiLCJCb1JvbGVzIjpbXSwiRW1yUm9sZXMiOls0LDYsMTAxLDksMCwxLDUsMywyLDgsMTAsMTFdLCJVc2VySWQiOjE0LCJHcm91cE1lbWJlcklkIjoxOCwiQ29tcGFueUlkIjoxLCJTZXNzaW9uSWQiOjQ1NTU4LCJMb2dpbk1ldGhvZCI6MCwiY2FwdGNoYVNjb3JlIjpudWxsfQ.869cwFHZjNg-47iW6mTR8ZJqbMQuNiVbKjQ5O_z-HP4";

        String response = ResponseHandler.getGetApiResponse(acceptHeader,
                requestUrl,
                "",
                200,
                authorizationToken).asString();
        System.out.println("Response: " + response);
    }
}
