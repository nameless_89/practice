package environment;

import com.browserup.bup.BrowserUpProxy;
import io.github.bonigarcia.wdm.WebDriverManager;

import static com.codeborne.selenide.Selenide.closeWebDriver;
import static com.codeborne.selenide.Selenide.open;
public class EnvironmentSetup {

    public BrowserUpProxy bmp;

    public void setUp() {
        System.setProperty("chromeoptions.mobileEmulation", "deviceName=Nexus 5");
        WebDriverManager.chromedriver().setup();
        /*Configuration.headless = true;*/
        open("https://test.fitekin.com/login/");
    }

    public void tearDown() {
        closeWebDriver();
    }
}
